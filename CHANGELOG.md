# Changelog
All notable changes to this project will be documented in this file.

## [0.0.0] - 2020-07-30
### Added
- Added changelog file
- Added .gitignore file

### Changed
- Changed package version to 0.0.0 to indicate artifact should not be considered stable
